<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class testMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $user = Auth::user();
        if($request->server->get('REQUEST_URI') == "/route-2" && ($user->role == 1 || $user->role == 2)){
            return $next($request);
        }
        else if($request->server->get('REQUEST_URI') == "/route-1" && ($user->role == 2)){
            return $next($request);
        }else if($request->server->get('REQUEST_URI') == "/route-3"){
            return $next($request);
        }
        abort(403);
    }
}
