<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Return_;

class TestController extends Controller
{

    public function __construct()
    {
        $this->middleware('test');
    }
    public function test1(){
        return "Selamat datang Super Admin";
    }
    public function test2(){
        return "Selamar datang Admin/Super Admin";
    }
    public function test3(){
        return "Selamat datang Guest/Admin/Super Admin";
    }
}
